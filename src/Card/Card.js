import React from 'react';
import './Card.css';

const Card = props => {
    return (
        <div>
            <div className={`Card Card-rank-${props.rank} Card-${props.suit}`}>
                <span className="Card-rank">{props.rank}</span>
                <span className="Card-suit">{props.suitIcon}</span>
            </div>
        </div>
    );
};

export default Card;