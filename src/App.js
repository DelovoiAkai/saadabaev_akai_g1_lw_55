import React, {Component, Fragment} from 'react';
import Card from "./Card/Card";
import "./App.css"

class App extends Component {
    state = {
        suit: [
            {"hearts": "♥"},
            {"diams": "♦"},
            {"spades": "♠"},
            {"clubs": "♣"}
        ],
    CardDeck: [
        {"hearts": "2"},
        {"diams": "3"},
        {"spades": "4"},
        {"clubs": "5"},
        {"hearts": "6"},
        {"diams": "7"},
        {"spades": "8"},
        {"clubs": "9"},
        {"hearts": "10"},
        {"diams": "j"},
        {"spades": "d"},
        {"clubs": "k"},
        {"hearts": "t"}
    ]
    };
    render() {
        return (
            <Fragment>
                <button id="Deal">Deal Card</button>
                <div className="App">
                    <Card suit="hearts" rank="k" suitIcon={this.state.suit[0].hearts}/>
                    <Card suit="diams" rank="7" suitIcon={this.state.suit[1].diams}/>
                    <Card suit="spades" rank="10" suitIcon={this.state.suit[2].spades}/>
                    <Card suit="clubs" rank="k" suitIcon={this.state.suit[3].clubs}/>
                    <Card suit="hearts" rank="2" suitIcon={this.state.suit[0].hearts}/>
                </div>
            </Fragment>


        );
    }
}

export default App;


